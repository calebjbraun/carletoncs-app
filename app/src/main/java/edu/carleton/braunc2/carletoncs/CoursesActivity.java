/**
 * CoursesActivity.java
 * Hami Abdi and Caleb Braun, 8 April 2015
 *
 * CoursesActivity displays the CS website listing all of the courses.
 *
 */

package edu.carleton.braunc2.carletoncs;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class CoursesActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courses_viewer);

        // Set up a WebView
        WebView coursesWebView = (WebView) findViewById(R.id.webview);
        WebSettings webSettings = coursesWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        // Create a spinner. It is declared final so that the following inner class can access it.
        final ProgressBar spinner = (ProgressBar)this.findViewById(R.id.loading_spinner);
        spinner.setVisibility(View.VISIBLE);

        // A subclass that is called when the WebView is finished loading
        coursesWebView.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                spinner.setVisibility(View.INVISIBLE);
            }
        });

        // Loads the page URL
        coursesWebView.loadUrl("https://apps.carleton.edu/curricular/cs/courses/");


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            this.finish();
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
