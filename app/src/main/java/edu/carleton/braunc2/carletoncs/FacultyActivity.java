/**
 * FacultyActivity.java
 * Hami Abdi and Caleb Braun, 8 April 2015
 *
 * FacultyActivity displays the faculty of the CS department.  It features their name, picture, and title.
 *
 */

package edu.carleton.braunc2.carletoncs;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class FacultyActivity extends Activity implements OnClickListener {

    int slideshowIndex = 0;
    private final int NUMBER_OF_FACULTY = 8;

    // An array of everything necessary for the slideshow
    private int[][][] nameIdsTitles = { {
            {R.string.acsi, R.drawable.acsi, R.string.chair},
            {R.string.aexl, R.drawable.aexl, R.string.visiting},
            {R.string.sgoi, R.drawable.sgoi, R.string.assprof},
            {R.string.dlib, R.drawable.dlib, R.string.assprof},
            {R.string.jmil, R.drawable.jmil, R.string.visiting},
            {R.string.dmus, R.drawable.dmus, R.string.prof},
            {R.string.jond, R.drawable.jond, R.string.prof},
            {R.string.araf, R.drawable.araf, R.string.prof}
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faculty_image_viewer);

        // Create forwards and backwards buttons
        Button backButton = (Button) findViewById(R.id.back_button);
        backButton.setOnClickListener(this);
        Button nextButton = (Button) findViewById(R.id.next_button);
        nextButton.setOnClickListener(this);

        // Initializes the first image
        updateView();

    }

    private void updateView(){
        // Update the image
        ImageView image = (ImageView) findViewById(R.id.myImage);
        image.setImageResource(nameIdsTitles[0][slideshowIndex][1]);

        // Update the text
        TextView nameText = (TextView)findViewById(R.id.textView);
        nameText.setText(nameIdsTitles[0][slideshowIndex][0]);

        // Update the titles
        TextView titleText = (TextView)findViewById(R.id.textView2);
        titleText.setText(nameIdsTitles[0][slideshowIndex][2]);
    }

    // Increases or decreases the image index depending on which button was pressed
    public void onClick(View view) {
        if (view.getId() == R.id.next_button) {
            // If the next button was clicked
            slideshowIndex++;
            if (slideshowIndex == NUMBER_OF_FACULTY) {
                slideshowIndex = 0;
            }
        }
        if (view.getId() == R.id.back_button) {
            // If the back button was clicked
            slideshowIndex--;
            if (slideshowIndex <= -1) {
                slideshowIndex = NUMBER_OF_FACULTY -1;
            }
        }
        updateView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}


