/**
 * HomeListActivity.java
 * Hami Abdi and Caleb Braun, 8 April 2015
 *
 * The HomeListActivity is the main activity of the app.  It displays a list that navigates to
 * various other activities.
 *
 */

package edu.carleton.braunc2.carletoncs;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;


public class HomeListActivity extends ListActivity {
    public static final int FACULTY_ACTIVITY= 0;
    public static final int COURSES_ACTIVITY= 1;
    public static final int MAJOR_ACTIVITY= 2;
    public static final int FACULTY_PROFILE_ACTIVITY= 3;
    public static final int LOCATION_ACTIVITY=4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Essentially create a ListView for our home page
        // allowing access to the various components of
        // the CS department.
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_list);

        ArrayList<String> listItems = new ArrayList<String>();
        listItems.add(getResources().getString(R.string.action_faculty));
        listItems.add(getResources().getString(R.string.action_courses));
        listItems.add(getResources().getString(R.string.action_major));
        listItems.add(getResources().getString(R.string.action_profile));
        listItems.add(getResources().getString(R.string.action_maps));

        // Creates the adapter for the ListView
        HomeListAdapter adapter = new HomeListAdapter(this, R.layout.list_cell, listItems);
        ListView listView = (ListView)this.findViewById(android.R.id.list);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflates the menu
        getMenuInflater().inflate(R.menu.menu_home_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handles action bar item clicks.
        int id = item.getItemId();

        // If the about button is clicked
        if (id == R.id.action_about) {
            Intent intent = new Intent(this, AboutPageActivity.class);
            startActivity(intent);

            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // If a list item is clicked, transition to that activity
    public void onListItemClick(ListView parent, View v, int position, long id) {
        Intent intent = null;
        if (id == FACULTY_ACTIVITY) {
            intent = new Intent(this, FacultyActivity.class);
        } else if (id == COURSES_ACTIVITY) {
            intent = new Intent(this, CoursesActivity.class);
        } else if (id == MAJOR_ACTIVITY) {
            intent = new Intent(this, MajorActivity.class);
        } else if (id == FACULTY_PROFILE_ACTIVITY) {
            intent = new Intent(this, StarFacultyActivity.class);
        } else if (id == LOCATION_ACTIVITY){
            intent = new Intent(this, LocationActivity.class);

        }

        startActivity(intent);
        // Change the transition animation
        overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
    }
}
