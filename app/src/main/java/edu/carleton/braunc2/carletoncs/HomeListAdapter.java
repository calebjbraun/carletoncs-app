/**
 * HomeListAdapter.java
 * Hami Abdi and Caleb Braun, 8 April 2015
 *
 * HomeListAdapter supports the HomeListActivity and creates the list.  Adapted from Jeff Ondich.
 *
 */

package edu.carleton.braunc2.carletoncs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;


public class HomeListAdapter extends ArrayAdapter<String> {
    public HomeListAdapter(Context context, int resource, ArrayList<String> listItems) {
        super(context, resource, listItems);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_cell, null);
        }

        if (v != null) {
            TextView textView = (TextView) v.findViewById(R.id.listCellTextView);
            textView.setText(this.getItem(position));
        }

        return v;
    }
}

