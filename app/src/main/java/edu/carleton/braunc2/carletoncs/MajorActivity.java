/**
 * MajorActivity.java
 * Hami Abdi and Caleb Braun, 8 April 2015
 *
 * MajorActivity displays a list of all of the courses required for the CS major.
 * The user selects the courses they have taken already, and they are notified how
 * many courses they still have to take.
 *
 */

package edu.carleton.braunc2.carletoncs;

import android.app.Activity;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;


public class MajorActivity extends Activity {
    public static final int TOTAL_COURSES = 11;
    public static int courses_left = 11;
    ListView classList;
    Button classesLeft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //create a ListView, similar to the one from Jeff's Sample Code
        //however, for this particular activity, we use a multiple choice list view
        //to allow the user to select multiple things

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_major_viewer);

        String[] listContent = getResources().getStringArray(R.array.cs_classes);

        classList = (ListView)findViewById(R.id.list);
        classesLeft = (Button)findViewById(R.id.classesLeft);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, listContent);
        classList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        classList.setAdapter(adapter);

        classesLeft.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View v){
                SparseBooleanArray classesArray = classList.getCheckedItemPositions();
                for(int i=0; i<TOTAL_COURSES+1;i++){
                    if (classesArray.get(i)){
                        courses_left--;
                    }
                }
                Toast.makeText(getApplicationContext(), Integer.toString(courses_left) + " Courses Left!", Toast.LENGTH_LONG).show();
                courses_left = 11; //todo don't change constants. caps = constant
            }

        });

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (id == android.R.id.home) {
            this.finish();
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}