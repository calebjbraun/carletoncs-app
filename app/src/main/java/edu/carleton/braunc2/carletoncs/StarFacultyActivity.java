/**
 * StarFacultyActivity.java
 * Hami Abdi and Caleb Braun, 8 April 2015
 *
 * StarFacultyActivity features a prominent member of the computer science department.
 *
 */

package edu.carleton.braunc2.carletoncs;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class StarFacultyActivity extends Activity implements OnClickListener {

    int slideshowIndex = 0;
    private final int NUMBER_OF_PICTURES = 3;

    // An array of the pictures in the slideshow
    private int[] pics = {
            R.drawable.star1,
            R.drawable.star2,
            R.drawable.star3,
            //R.drawable.star4
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_star_faculty);

        // Create forward and backwards buttons
        Button backButton = (Button) findViewById(R.id.back_button);
        backButton.setOnClickListener(this);
        Button nextButton = (Button) findViewById(R.id.next_button);
        nextButton.setOnClickListener(this);

        // Initializes first image
        updateImage();

    }

    // Updates the image on screen
    private void updateImage(){
        ImageView image = (ImageView) findViewById(R.id.myImage);
        image.setImageResource(pics[slideshowIndex]);
    }

    // Increases or decreases the image index depending on which button was pressed
    public void onClick(View view) {
        // If the next button was clicked
        if (view.getId() == R.id.next_button) {
            slideshowIndex++;
            // Loop back if at end of slideshow
            if (slideshowIndex > NUMBER_OF_PICTURES-1) {
                slideshowIndex = 0;
            }
        }
        // If the next button was clicked
        if (view.getId() == R.id.back_button) {
            slideshowIndex--;
            // Loop to end if at beginning of slideshow
            if (slideshowIndex <= -1) {
                slideshowIndex = NUMBER_OF_PICTURES -1;
            }
        }
        updateImage();
    }

    // Returns to HomeListActivity when the home button is pressed
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}